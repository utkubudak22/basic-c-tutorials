#include <iostream>

using namespace std;

struct Time{

int hour;
int minute;
int second;

};

int main(){
	
	Time time1, time2, resultant;
	
	cout << "Please enter the hours for time: 1: ";
	cin >> time1.hour;
	
	cout << "Please enter the minutes for time 1: ";
	cin >> time1.minute;

    cout << "Please enter the seconds for time 1: ";
	cin >> time1.second;
	
	cout << endl;
	
	cout << "Please enter the hours for time: 2: ";
	cin >> time2.hour;
	
	cout << "Please enter the minutes for time 2: ";
	cin >> time2.minute;

    cout << "Please enter the seconds for time 2: ";
	cin >> time2.second;
	
	resultant.second = (time1.second + time2.second) % 60;
	
	resultant.hour = ((time1.hour + time2.hour+ ((time1.hour + time2.hour) / 24))) % 24;
	resultant.minute = ((time1.minute + time2.minute + ((time1.second + time2.second) / 60 ))) % 60;
	resultant.second = (time1.second + time2.second) % 60;
	
	cout << endl;
	
	cout << "The total time is " << resultant.hour << ":" << resultant.minute 
	<< ":" << resultant.second;
		
	return 0;
	
}
