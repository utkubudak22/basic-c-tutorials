#include <iostream>

using namespace std;

int main(){
	
	double array1[20] = { };
	double array2[19] = { };
	
	for(int i = 0 ; i < 20 ; i++){
		
		if(i == 0){
			
			array1[i] = 1;
			
		}else if(i == 1){
			
			array1[i] = 1;
			
		}else{
			
			array1[i] = array1[i-1] + array1[i-2];
						
		}
				
	}
	
	for(int i = 0; i < 19; i++){
		array2[i] = (array1[i] + array1[i+1]) / 2; 
	}
	
	cout << "The first 20 Fibonacci series numbers are:" << endl;
	
	for(int i = 0; i < 20; i++){
		cout << array1[i] << "\n";
	}
	
	cout << "The averaged adjacent array numbers are:" << endl;
	
	for(int i = 0; i < 19; i++){
		cout << array2[i] << "\n";
	}
			
	return 0;
	
}
