#include <iostream>

using namespace std;

int main(){
	
	int unsortedArray[10] = { };
	
	for(int i = 0; i < 10; i++){
	
		cout << "Please enter an integer value for array index " << i+1 << endl;
		cin >> unsortedArray[i];
	
	}	
	
	for(int i = 0; i < 10; i++){
		
		for(int j = i; j < 10; j++){
			
			if(unsortedArray[i] > unsortedArray[j]){
			
				int temp = unsortedArray[i];
				unsortedArray[i] = unsortedArray[j];
				unsortedArray[j] = temp;
		}
		
		}
				
	}
	
	cout << "The sorted array is:\n";
	
	for(int i=0; i < 10; i++){
		cout << unsortedArray[i] << "\n";
	}
	
	return 0;
	
}
