#include <iostream>

using namespace std;

int main(){
	
	int number1, number2;
	
	cout << "Enter Number 1: ";
	cin >> number1;
	
	cout << "Enter Number 2: ";
	cin >> number2;
	
	if(number1 >= number2){
		cout << "Result: " << number1 - number2;
	}
	else if(number1 < number2){
		cout << "Result: " << number2 - number1;
	}
	
	return 0;
		
}


