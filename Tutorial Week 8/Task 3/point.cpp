#include "point.h"
#include <iostream>
#include <cmath>

using namespace std;

void Point::print()
{
cout << "The x value is: " << x_axis << "\n" << "The y value is: " << y_axis << "\n" << "The z value is: " << z_axis << endl;
}

float Point::norm()
{
	float temp = pow(x_axis,2) + pow(y_axis,2) + pow(z_axis,2);
	return sqrt(temp);
}

void Point::negate()
{
	x_axis = -1 * x_axis;
	y_axis = -1 * y_axis;
	z_axis = -1 * z_axis;
		
}

void Point::set (float x, float y, float z)
{
	x_axis = x;
	y_axis = y;
	z_axis = z;
}

