#ifndef POINT_H 
#define POINT_H 

class Point
{
public:
	
	Point (float x, float y, float z)
	{
	x_axis = x;
	y_axis = y;
	z_axis = z;
	}
	
	void print ();
	float norm ();
	void negate ();
	void set (float x, float y, float z);

private:
	float x_axis, y_axis, z_axis;

};

#endif

