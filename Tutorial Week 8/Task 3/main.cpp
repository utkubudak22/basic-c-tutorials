#include <iostream>
#include "point.h"
#include <cmath>

using namespace std;


class Advanced_Point: public Point 
{
public:
	
	Advanced_Point(float x ,float y,float z) : Point(x,y,z){}	
	
	reset(){
	
		set (0,0,0);
	
	}

};

int main(int argc, char** argv) {
Advanced_Point A1(1,2,3);	
int x, y, z;
cout << "Please provide X, Y, Z values:" << endl;
cin >> x >> y >> z;
A1.set(x,y,z);
cout << "The function prints out: \n";
A1.print ();
cout << "The result of norm is: " << A1.norm () << endl;
A1.negate ();
cout << "The result of negate: \n";
A1.print ();
A1.reset ();
cout << "After reset, the function prints out: \n";
A1.print ();
return 0;

}
