#ifndef POINT_H 
#define POINT_H 

class Point
{
public:
	
	Point ()
	{
	}
	
	void print ();
	float norm ();
	void negate ();
	void set (float x, float y, float z);

private:
	float x_axis, y_axis, z_axis;

};

#endif

