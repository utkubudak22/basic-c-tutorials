#include <iostream>
#include <cmath>

using namespace std;

class Point
{
public:
	
	Point (float x, float y, float z)
	{
	x_axis = x;
	y_axis = y;
	z_axis = z;
	}
	
	void print ();
	float norm ();
	void negate ();

private:
	float x_axis, y_axis, z_axis;

};

void Point::print()
{
cout << "The x value is: " << x_axis << "\n" << "The y value is: " << y_axis << "\n" << "The z value is: " << z_axis << endl;
}

float Point::norm()
{
	float temp = pow(x_axis,2) + pow(y_axis,2) + pow(z_axis,2);
	return sqrt(temp);
}

void Point::negate()
{
	x_axis = -1 * x_axis;
	y_axis = -1 * y_axis;
	z_axis = -1 * z_axis;
		
}

int main ()
{
Point A1(1,2,3);
cout << "The function prints out: \n";
A1.print ();
cout << "The result of norm is: " << A1.norm () << endl;
A1.negate ();
cout << "The result of negate: \n";
A1.print ();
return 0;
}

