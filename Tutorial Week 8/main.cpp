#include <iostream>
#include "point.h"
#include <cmath>

using namespace std;

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	
int x, y, z;
cout << "Please provide X, Y, Z values:" << endl;
cin >> x >> y >> z;
Point A1;
A1.set(x,y,z);
cout << "The function prints out: \n";
A1.print ();
cout << "The result of norm is: " << A1.norm () << endl;
A1.negate ();
cout << "The result of negate: \n";
A1.print ();
return 0;

}
