#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

int main(){
	
	int numberChoice;
	string filename;
	ofstream testFile;
	
	cout << "Please enter a filename: ";
	cin >> filename;
	const char *c = filename.c_str();
	
	cout << "Please indicate how many float numbers you want to enter: ";
	cin >> numberChoice;
	
	testFile.open(c);
	
	for(int i = 0; i < numberChoice; i++){
		
		float number;
		
		cout << "Please enter a number: ";
		cin >> number;
		
		testFile.setf(ios::fixed);
		testFile.precision(2);
		testFile.fill('0');
		testFile.width(7);
		testFile << number << "\n";
		
	}
	
	testFile.close();
	
	cout << "Your numbers have been saved." ;
	
	return 0;
	
}
