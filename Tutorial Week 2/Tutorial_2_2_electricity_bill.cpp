#include <iostream>

using namespace std;

int main(){
	
	double cur_peak = 0.0,pas_peak = 0.0;
	double cur_offpeak = 0.0,pas_offpeak = 0.0;
	double dif_peak = 0.0,dif_offpeak = 0.0;
	double bill = 0.0;
	
	cout << "Enter current peak meter reading: ";
	cin >> cur_peak;
	cout << "Enter past peak meter reading: ";
	cin >> pas_peak;
	dif_peak = cur_peak - pas_peak;
	
	cout << "Enter current off-peak meter reading: ";
	cin >> cur_offpeak;
	cout << "Enter past off-peak meter reading: ";
	cin >> pas_offpeak;
	dif_offpeak = cur_offpeak - pas_offpeak;
	
	bill = (((dif_peak*5.6) + (dif_offpeak*2.2)) / 100) + 8.50;
	
	cout << "Your electricity bill is: " << bill << " $";
		
	return 0;
}
