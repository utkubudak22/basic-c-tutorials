#include <iostream>

using namespace std;

void sportsTimer(int seconds);

int main(){
	
	int x;
	
	cout << "Enter the time for the event in seconds: " <<endl;
	cin >> x;
	
	sportsTimer(x);
	
	return 0;
	
}

void sportsTimer(int seconds){
	
	int hour = seconds / 3600;
	
	int temp1 = seconds % 3600;
	int minute = temp1 / 60; 
	
	int temp2 = temp1 % 60;
	int second = temp2;
	
	cout << "The number of hours is " << hour << endl;
	cout << "The number of minutes is " << minute << endl;
	cout << "The number of seconds is " << second << endl;	
		
}

