#include <iostream>

using namespace std;

//function declaration
void swap(int ra, int rb);

main()
{
	
int a = 10, b = 20;
cout << "Before swapping:\n";
cout << a <<" "<< b << "\n";
cout << "After swapping:\n";
swap (a,b); // Call function swap

}

//function definition
//reference variables ra and rb are the function arguments
//No value is returned via type specifier
void swap(int ra, int rb)
{
	
int temp;
temp = ra;
ra = rb;
rb = temp;
cout << ra << " " << rb;

}

