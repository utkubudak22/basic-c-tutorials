#include <iostream>

using namespace std;

//function declaration
void swap(int &ra, int &rb);

struct Numbers{

int value;

};

main()
{
	
Numbers number1, number2;
	
number1.value = 10, number2.value = 20;
cout << "Before swapping:\n";
cout << number1.value <<" "<< number2.value << "\n";
swap (number1.value,number2.value); // Call function swap
cout << "After swapping:\n";
cout << number1.value << " " << number2.value;

}

//function definition
//reference variables ra and rb are the function arguments
//No value is returned via type specifier
void swap(int &ra, int &rb)
{
int temp;
temp = ra;
ra = rb;
rb = temp;
}
