#include <iostream>

#include "calcaverage.h"

using namespace std;

int main(){
	
	double x, y, average;
	
	cout << "Enter number 1: ";
	cin >> x;
	
	cout << "Enter number 2: ";
	cin >> y;
	
	average = calcAverage(x, y);
	
	cout << "The average of " << x << " and " << y << " is: "
	<< average;
		
	return 0;
	
}


