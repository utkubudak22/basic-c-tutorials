#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	
	int journeyTime, firstTrainTime, lastTrainTime, frequency;
	int hourFirst, minuteFirst, hourLast, minuteLast;
	int first, last;
	
	do{
		
	cout << "Enter the journey time (minutes)" << endl;
	cin >> journeyTime;
	
	} while(journeyTime < 0 || journeyTime > 60);
	
	do{
	
	cout << "Enter the first train time" << endl;
	cin >> firstTrainTime;
	
	} while(firstTrainTime < 0 || firstTrainTime > 2400);
	
	do{
	
	cout << "Enter the last train time" << endl;
	cin >> lastTrainTime;
	
	} while(lastTrainTime < 0 || lastTrainTime > 2400);
	
	do{
		
	cout << "Enter the frequency time" << endl;
	cin >> frequency;
	
	} while(frequency < 0 || frequency > 60);
	
	cout << "Depart" << "\tDepart" << "\tDepart" << "\tDepart" << "\tArrive" << endl;
	cout << "RED" << "\tGREEN" << "\tYELLOW" << "\tORANGE" << "\tBLUE" << endl;
    
	hourFirst = firstTrainTime / 100;
	minuteFirst = firstTrainTime % 100;
	
	first = hourFirst * 60 + minuteFirst;
	
	hourLast = lastTrainTime / 100;
	minuteLast = lastTrainTime % 100; 
	
	last = hourLast * 60 + minuteLast;
	
	while(first <= last){
		
		cout << first/60 << setfill('0') << setw(2) <<first%60 
		<< "\t" << (first+journeyTime+2)/60 
		<< setfill('0') << setw(2) << (first+journeyTime+2)%60 << "\t" << 
		setfill('0') << setw(2) << (first+2*journeyTime+4)/60 << setfill('0') 
		<< setw(2) << (first+2*journeyTime+4)%60<< "\t" << setfill('0') << setw(2) 
		<< (first+3*journeyTime+6)/60 << setfill('0') << setw(2) << (first+3*journeyTime+6)%60 << 
		"\t" << setfill('0') << setw(2) << (first+4*journeyTime+6)/60 << setfill('0') 
		<< setw(2) << (first+4*journeyTime+6)%60 << "\n";
		
		first = first + frequency;
	}
	
	return 0;
}
