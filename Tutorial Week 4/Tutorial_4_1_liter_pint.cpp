#include <iostream>

using namespace std;

int main(){
	
	double startLiter, endLiter, interval;
	
	cout << "Please enter the start amount of liters: ";
	cin >> startLiter;
	
	cout << "Please enter the end amount of liters: ";
	cin >> endLiter;
	
	cout << "Please enter the interval: ";
	cin >> interval;
	
	cout << "LITERS" << "  " << "PINS\n";
	cout << "==============" << "\n";
	
	double i = startLiter; 
	
	while(i <= endLiter){
		
		cout << i << "\t" << i * 1.76 << "\n";
		
		i = i + interval;
	}
	
	cout << "Done.";
		
	return 0;
}
